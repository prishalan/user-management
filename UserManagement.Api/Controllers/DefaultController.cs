﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace UserManagement.Api.Controllers
{
    public class DefaultController : ApiController
    {
        public DefaultController()
        {

        }

        [HttpGet]
        public IHttpActionResult Index()
        {
            var baseHref = System.Configuration.ConfigurationManager.AppSettings["BaseHref"];
            return Ok(new
            {
                name = "RewardsCo User Management API",
                version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                docs = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + baseHref + "swagger"
            });
        }
    }
}
