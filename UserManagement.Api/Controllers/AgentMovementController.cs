﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserManagement.Core.Services;

namespace UserManagement.Api.Controllers
{
    public class AgentMovementController : ApiController
    {
        private readonly UserService _userService;

        public AgentMovementController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IHttpActionResult GetJuniorManagers()
        {
            //try
            //{
            //    var jobTitle = System.Configuration.ConfigurationManager.AppSettings["JuniorManagerJobTitle"];
            //    var result = _userService.GetActiveJuniorManagers(jobTitle);

            //    if (!result.Success)
            //    {
            //        return BadRequest("Something went wrong");
            //    }

            //    return Ok(result.Data);
            //}
            //catch (Exception)
            //{

            //    throw;
            //}
            return Ok(new[] {
                new { id = 2, name = "John Wick" },
                new { id = 6, name = "Jessica Alba" },
                new { id = 19, name = "Kevin Bacon" },
                new { id = 21, name = "Mark Wahlberg" }
            });
        }


        [HttpGet]
        public IHttpActionResult GetJuniorManagerCampaigns(int id)
        {
            var campaigns = new[]
            {
                new { id = 1, title = "MO_V_FLEXISIMONLY_UAT", code = "MO_V_FLEXISIMONLY_UAT", owner = 2 },
                new { id = 2, title = "VO_LG_LEADGENERATION_UAT", code = "VO_LG_LEADGENERATION_UAT", owner = 2  },
                new { id = 3, title = "VO_LG_OAKHURST_UAT", code = "VO_LG_OAKHURST_UAT", owner = 6  },
                new { id = 4, title = "VO_OH_ROADPROTECT_UAT", code = "VO_OH_ROADPROTECT_UAT", owner = 6  },
                new { id = 5, title = "VO_TJ_EasyFix_UAT", code = "VO_TJ_EasyFix_UAT", owner = 19  },
                new { id = 6, title = "MO_T_NewLineVoice_UAT", code = "MO_T_NewLineVoice_UAT", owner = 19  },
                new { id = 7, title = "VO_LG_1LifeFuneral_UAT", code = "VO_LG_1LifeFuneral_UAT", owner = 19  },
                new { id = 8, title = "MO_V_Broadband_UAT", code = "MO_V_Broadband_UAT", owner = 21  }
            };

            var ownedCampaigns = campaigns.Where(x => x.owner == id);

            return Ok(ownedCampaigns);
        }


        [HttpGet]
        public IHttpActionResult GetCampaignAgents(int id)
        {
            var agents = new[]
            {
                new { id = 1, name = "Bertha Hood", campaign = 1 },
                new { id = 2, name = "Nicolle Bellamy", campaign = 1 },
                new { id = 3, name = "Edward Davis", campaign = 1 },
                new { id = 4, name = "Yasmin Stevens", campaign = 1 },
                new { id = 5, name = "Ariel Flynn", campaign = 1 },
                new { id = 7, name = "Marie Gould", campaign = 2 },
                new { id = 8, name = "Habib Schroeder", campaign = 2 },
                new { id = 9, name = "Luna Hopkins", campaign = 2 },
                new { id = 10, name = "Nazia Crane", campaign = 2 },
                new { id = 11, name = "Ralph Beil", campaign = 2 },
                new { id = 12, name = "Kamil Webber", campaign = 2 },
                new { id = 13, name = "Farah Larson", campaign = 2 },
                new { id = 14, name = "Juanita Austin", campaign = 3 },
                new { id = 15, name = "Honor Naylor", campaign = 3 },
                new { id = 16, name = "Sonya Terrell", campaign = 3 },
                new { id = 17, name = "Aniyah Cisneros", campaign = 3 },
                new { id = 18, name = "Ebony Rush", campaign = 3 },
                new { id = 20, name = "June Humphries", campaign = 3 },
                new { id = 22, name = "Nazifa Hammond", campaign = 3 },
                new { id = 23, name = "Jazmine Flores", campaign = 3 },
                new { id = 24, name = "Sam Estes", campaign = 3 },
                new { id = 25, name = "Myah Aguirre", campaign = 3 },
                new { id = 26, name = "Kobi Kavanagh", campaign = 4 },
                new { id = 27, name = "Javier Suarez", campaign = 4 },
                new { id = 28, name = "Tomos Riggs", campaign = 4 },
                new { id = 29, name = "Elissa Elliott", campaign = 4 },
                new { id = 30, name = "Myron Bassett", campaign = 4 },
                new { id = 31, name = "Eddie Workman", campaign = 4 },
                new { id = 32, name = "Eleri Miller", campaign = 4 },
                new { id = 33, name = "Cheyanne Burt", campaign = 4 },
                new { id = 34, name = "Renesmae Mcdonnell", campaign = 4 },
                new { id = 35, name = "Montgomery Swanson", campaign = 4 },
                new { id = 36, name = "Dahlia Knight", campaign = 4 },
                new { id = 37, name = "Roseanna Mcclure", campaign = 4 },
                new { id = 38, name = "Dulcie Burris", campaign = 4 },
                new { id = 39, name = "Nikita Flores", campaign = 4 },
                new { id = 40, name = "Nora Matthews", campaign = 5 },
                new { id = 41, name = "Hoorain Johns", campaign = 5 },
                new { id = 42, name = "Kylo Hussain", campaign = 5 },
                new { id = 43, name = "Jayde Mcdonald", campaign = 5 },
                new { id = 44, name = "Iram George", campaign = 5 },
                new { id = 45, name = "Jia Green", campaign = 5 },
                new { id = 46, name = "Cyrus Norton", campaign = 6 },
                new { id = 47, name = "Montel Eaton", campaign = 6 },
                new { id = 48, name = "Ira Craft", campaign = 6 },
                new { id = 49, name = "Holly Delacruz", campaign = 7 },
                new { id = 50, name = "Claire Travers", campaign = 7 },
                new { id = 51, name = "Mahi Hayes", campaign = 7 },
                new { id = 52, name = "Danika Hammond", campaign = 7 },
                new { id = 53, name = "Elsie-Mae Peel", campaign = 7 },
                new { id = 54, name = "Craig Benton", campaign = 7 },
                new { id = 55, name = "Koa Lu", campaign = 7 },
                new { id = 56, name = "Danny Hewitt", campaign = 7 },
                new { id = 57, name = "Devonte O'Reilly", campaign = 8 },
                new { id = 58, name = "Subhaan Mcknight", campaign = 8 },
                new { id = 59, name = "Raja Jefferson", campaign = 8 },
                new { id = 60, name = "Lorraine Clifford", campaign = 8 },
            };

            var campaignAgents = agents.Where(x => x.campaign == id);

            return Ok(campaignAgents);
        }
    }
}
