using System.Web.Http;
using Unity;
using Unity.WebApi;
using UserManagement.Core.Persistence;
using UserManagement.Infrastructure.Persistence;

namespace UserManagement.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IUnitOfWorkFactory, UnitOfWorkFactory>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}