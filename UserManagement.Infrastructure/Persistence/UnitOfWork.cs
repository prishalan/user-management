﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Persistence;
using UserManagement.Core.Repository;

namespace UserManagement.Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Contexts.UserManagementDbContext _context;

        public UnitOfWork(Contexts.UserManagementDbContext context)
        {
            _context                    = context;
            ApiUsers                    = new Repository.ApiUserRepository(_context);
            Campaigns                   = new Repository.CampaignRepository(_context);
            Companies                   = new Repository.CompanyRepository(_context);
            Departments                 = new Repository.DepartmentRepository(_context);
            JobTitles                   = new Repository.JobTitleRepository(_context);
            ManagedOrganisationalUnits  = new Repository.ManagedOrganisationalUnitRepository(_context);
            OrganisationalUnits         = new Repository.OrganisationalUnitRepository(_context);
            ThirdPartyPlatforms         = new Repository.ThirdPartyPlatformRepository(_context);
            UserMovementTransactions    = new Repository.UserMovementTransactionRepository(_context);
            Users                       = new Repository.UserRepository(_context);
            UserThirdPartyLogins        = new Repository.UserThirdPartyLoginRepository(_context);
        }

        public IApiUserRepository ApiUsers { get; }

        public ICampaignRepository Campaigns { get; }

        public ICompanyRepository Companies { get; }

        public IDepartmentRepository Departments { get; }

        public IJobTitleRepository JobTitles { get; }

        public IManagedOrganisationalUnitRepository ManagedOrganisationalUnits { get; }

        public IOrganisationalUnitRepository OrganisationalUnits { get; }

        public IThirdPartyPlatformRepository ThirdPartyPlatforms { get; }

        public IUserMovementTransactionRepository UserMovementTransactions { get; }

        public IUserRepository Users { get; }

        public IUserThirdPartyLoginRepository UserThirdPartyLogins { get; }



        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
