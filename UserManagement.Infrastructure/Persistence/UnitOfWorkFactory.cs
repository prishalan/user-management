﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Persistence;

namespace UserManagement.Infrastructure.Persistence
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly Contexts.UserManagementDbContext _context;

        public UnitOfWorkFactory(Contexts.UserManagementDbContext context)
        {
            _context = context;
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork(_context);
        }
    }
}
