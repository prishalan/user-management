﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;
using UserManagement.Core.Repository;

namespace UserManagement.Infrastructure.Repository
{
    public class CompanyRepository : Base.GenericRepository<Company, int>, ICompanyRepository
    {
        public CompanyRepository(Contexts.UserManagementDbContext context) : base(context)
        {
        }
    }
}
