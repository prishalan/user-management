﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;
using UserManagement.Core.Repository;

namespace UserManagement.Infrastructure.Repository
{
    public class JobTitleRepository : Base.GenericRepository<JobTitle, int>, IJobTitleRepository
    {
        public JobTitleRepository(Contexts.UserManagementDbContext context) : base(context)
        {
        }
    }
}
