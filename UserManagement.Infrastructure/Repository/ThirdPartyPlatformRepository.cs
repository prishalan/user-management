﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;
using UserManagement.Core.Repository;

namespace UserManagement.Infrastructure.Repository
{
    public class ThirdPartyPlatformRepository : Base.GenericRepository<ThirdPartyPlatform, int>, IThirdPartyPlatformRepository
    {
        public ThirdPartyPlatformRepository(Contexts.UserManagementDbContext context) : base(context)
        {
        }
    }
}
