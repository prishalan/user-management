﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;
using UserManagement.Core.Repository;

namespace UserManagement.Infrastructure.Repository
{
    public class CampaignRepository : Base.GenericRepository<Campaign, int>, ICampaignRepository
    {
        public CampaignRepository(Contexts.UserManagementDbContext context) : base(context)
        {
        }
    }
}
