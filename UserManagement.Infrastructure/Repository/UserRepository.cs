﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;
using UserManagement.Core.Repository;

namespace UserManagement.Infrastructure.Repository
{
    public class UserRepository : Base.GenericRepository<User, int>, IUserRepository
    {
        public Contexts.UserManagementDbContext context
        {
            get
            {
                return _context as Contexts.UserManagementDbContext;
            }
        }

        public UserRepository(Contexts.UserManagementDbContext context) : base(context)
        {
        }

        public IEnumerable<User> GetAllUsersWithJobTitles()
        {
            return context.Users.Include(i => i.JobTitle).AsEnumerable();
        }
    }
}
