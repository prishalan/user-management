﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;

namespace UserManagement.Infrastructure.Contexts
{
    public class UserManagementDbContext : DbContext
    {
        public virtual DbSet<ApiUser> ApiUsers { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<JobTitle> JobTitles { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<ManagedOrganisationalUnit> ManagedOrganisationalUnits { get; set; }
        public virtual DbSet<OrganisationalUnit> OrganisationalUnits { get; set; }
        public virtual DbSet<ThirdPartyPlatform> ThirdPartyPlatforms { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserMovementTransaction> UserMovementTransactions { get; set; }
        public virtual DbSet<UserThirdPartyLogin> UserThirdPartyLogins { get; set; }


        public UserManagementDbContext() : base("name=DefaultConnection")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            Database.SetInitializer<DbContext>(null);
            Configuration.LazyLoadingEnabled = false;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new EntityConfiguration.ApiUserEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.CompanyEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.DepartmentEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.JobTitleEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.CampaignEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.ManagedOrganisationalUnitEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.OrganisationalUnitEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.ThirdPartyPlatformEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.UserEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.UserMovementTransactionEntityConfiguration());
            modelBuilder.Configurations.Add(new EntityConfiguration.UserThirdPartyLoginEntityConfiguration());
        }
    }
}
