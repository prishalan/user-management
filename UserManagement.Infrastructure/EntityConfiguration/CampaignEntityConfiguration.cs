﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class CampaignEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.Campaign, int>
    {
        public CampaignEntityConfiguration()
        {
            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(100);

            Property(p => p.Code)
                .IsRequired()
                .HasMaxLength(50);

            Property(p => p.Description)
                .IsOptional()
                .HasMaxLength(255);

            Property(p => p.IsActive)
                .IsRequired();

            ToTable("Campaigns", "usermgt");
        }
    }
}
