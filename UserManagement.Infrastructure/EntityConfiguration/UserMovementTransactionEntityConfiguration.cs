﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class UserMovementTransactionEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.UserMovementTransaction, long>
    {
        public UserMovementTransactionEntityConfiguration()
        {
            Property(p => p.Status)
                .IsRequired()
                .HasMaxLength(10); // use: UserManagement.Core.Types.TransactionStatus

            Property(p => p.EffectiveDateStart)
                .IsRequired();

            Property(p => p.EffectiveDateEnd)
                .IsRequired();

            Property(p => p.Note)
                .IsOptional()
                .HasMaxLength(255);


            HasRequired(r => r.User)
                .WithMany(m => m.UserMovementTransactions)
                .HasForeignKey(f => f.UserId);

            HasRequired(r => r.User)
                .WithMany(m => m.UserManagerMovementTransactions)
                .HasForeignKey(f => f.ManagerUserId);

            HasRequired(r => r.OrganisationalUnit)
                .WithMany(m => m.UserMovementTransactions)
                .HasForeignKey(f => f.OrganisationalUnitId);


            ToTable("UserMovementTransactions", "usermgt");
        }
    }
}
