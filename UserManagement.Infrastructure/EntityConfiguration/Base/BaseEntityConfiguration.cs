﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration.Base
{
    public abstract class BaseEntityConfiguration<TEntity, TType> : EntityTypeConfiguration<TEntity> 
        where TEntity : Core.Entities.Base.BaseAuditableEntity<TType> 
        where TType : struct
    {
        public BaseEntityConfiguration()
        {
            HasKey(k => k.Id);

            Property(p => p.CreatedBy)
                .IsRequired()
                .HasMaxLength(100);

            Property(p => p.CreatedDate)
                .IsRequired();

            Property(p => p.ModifiedBy)
                .IsRequired()
                .HasMaxLength(100);

            Property(p => p.ModifiedDate)
                .IsRequired();
        }
    }
}
