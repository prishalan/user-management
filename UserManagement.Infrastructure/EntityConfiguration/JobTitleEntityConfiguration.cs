﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class JobTitleEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.JobTitle, int>
    {
        public JobTitleEntityConfiguration()
        {
            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.IsActive)
                .IsRequired();

            ToTable("SupportJobTitles");
            //ToTable("SupportJobTitles", "usermgt");
        }
    }
}
