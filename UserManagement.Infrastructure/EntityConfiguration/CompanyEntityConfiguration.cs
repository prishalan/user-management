﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class CompanyEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.Company, int>
    {
        public CompanyEntityConfiguration()
        {
            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.IsActive)
                .IsRequired();

            ToTable("SupportCompanies", "usermgt");
        }
    }
}
