﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class UserThirdPartyLoginEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.UserThirdPartyLogin, int>
    {
        public UserThirdPartyLoginEntityConfiguration()
        {
            Property(p => p.Username)
                .IsRequired()
                .HasMaxLength(100);

            Property(p => p.IsActive)
                .IsRequired();


            HasRequired(r => r.User)
                .WithMany(m => m.UserThirdPartyLogins)
                .HasForeignKey(f => f.UserId);

            HasRequired(r => r.Platform)
                .WithMany(m => m.UserThirdPartyLogins)
                .HasForeignKey(f => f.PlatformId);


            ToTable("UserThirdPartyLogins", "usermgt");
        }
    }
}
