﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class ApiUserEntityConfiguration : EntityTypeConfiguration<Core.Entities.ApiUser>
    {
        public ApiUserEntityConfiguration()
        {
            HasKey(k => k.Id);

            Property(p => p.UserName)
                .IsRequired()
                .HasMaxLength(50);

            Property(p => p.Password)
                .IsRequired()
                .HasMaxLength(255);

            Property(p => p.IsActive)
                .IsRequired();

            ToTable("ApiUser", "usermgt");
        }
    }
}
