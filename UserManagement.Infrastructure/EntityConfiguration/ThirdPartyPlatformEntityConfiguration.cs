﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class ThirdPartyPlatformEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.ThirdPartyPlatform, int>
    {
        public ThirdPartyPlatformEntityConfiguration()
        {
            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.IsActive)
                .IsRequired();

            ToTable("ThirdPartyPlatforms", "usermgt");
        }
    }
}
