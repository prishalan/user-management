﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class DepartmentEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.Department, int>
    {
        public DepartmentEntityConfiguration()
        {
            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.IsActive)
                .IsRequired();

            ToTable("SupportDepartments", "usermgt");
        }
    }
}
