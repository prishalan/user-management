﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class OrganisationalUnitEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.OrganisationalUnit, int>
    {
        public OrganisationalUnitEntityConfiguration()
        {
            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(30);

            Property(p => p.LdapPath)
                .IsRequired()
                .HasMaxLength(255);

            Property(p => p.IsActive)
                .IsRequired();

            HasOptional(p => p.AssociatedCampaign)
                .WithMany(m => m.OrganisationalUnits)
                .HasForeignKey(f => f.AssociatedCampaignId);

            HasOptional(o => o.Parent)
                .WithMany(m => m.Children)
                .HasForeignKey(f => f.ParentId);

            ToTable("OrganisationalUnits", "usermgt");
        }
    }
}
