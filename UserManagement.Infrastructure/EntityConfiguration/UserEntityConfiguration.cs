﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class UserEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.User, int>
    {
        public UserEntityConfiguration()
        {
            Property(p => p.UserType)
                .IsRequired()
                .HasMaxLength(15); // use: UserManagement.Core.Types.UserType

            Property(p => p.EmployeeNumber)
                .IsRequired()
                .HasMaxLength(10);

            Property(p => p.Firstname)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.Middlename)
                .IsOptional()
                .HasMaxLength(64);

            Property(p => p.Lastname)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.IdentityNumber)
                .IsRequired()
                .HasMaxLength(13);

            Property(p => p.UserType)
                .IsOptional()
                .HasMaxLength(13);

            Property(p => p.PassportNumber)
                .IsOptional()
                .HasMaxLength(30);

            Property(p => p.Description)
                .IsOptional()
                .HasMaxLength(64);

            Property(p => p.ADCommonName)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.ADDisplayName)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.ADSamAccountName)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.EmailAddress)
                .IsRequired()
                .HasMaxLength(64);

            Property(p => p.TelephoneNumber)
                .IsOptional()
                .HasMaxLength(13);

            Property(p => p.MobileNumber)
                .IsOptional()
                .HasMaxLength(13);

            Property(p => p.IpPhoneNumber)
                .IsOptional()
                .HasMaxLength(13);

            Property(p => p.ManagerEffectiveDate)
                .IsOptional();

            Property(p => p.OrganisationalUnitEffectiveDate)
                .IsRequired();

            Property(p => p.TerminationDate)
                .IsRequired();

            Property(p => p.TerminationReason)
                .IsOptional()
                .HasMaxLength(150);

            Property(p => p.IsActive)
                .IsRequired();


            HasRequired(r => r.Company)
                .WithMany(m => m.Users)
                .HasForeignKey(f => f.CompanyId);

            HasRequired(r => r.Department)
                .WithMany(m => m.Users)
                .HasForeignKey(f => f.DepartmentId);

            HasRequired(r => r.JobTitle)
                .WithMany(m => m.Users)
                .HasForeignKey(f => f.JobTitleId);

            HasOptional(o => o.Manager)
                .WithMany(m => m.ManagedUsers)
                .HasForeignKey(f => f.ManagerId);

            HasRequired(o => o.OrganisationalUnit)
                .WithMany(m => m.Users)
                .HasForeignKey(f => f.OrganisationalUnitId);


            ToTable("Users");
            //ToTable("Users", "usermgt");
        }
    }
}
