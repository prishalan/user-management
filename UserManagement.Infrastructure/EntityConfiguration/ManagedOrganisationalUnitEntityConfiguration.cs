﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Infrastructure.EntityConfiguration
{
    public class ManagedOrganisationalUnitEntityConfiguration : Base.BaseEntityConfiguration<Core.Entities.ManagedOrganisationalUnit, int>
    {
        public ManagedOrganisationalUnitEntityConfiguration()
        {
            Property(p => p.EffectiveDate)
                .IsRequired();

            HasRequired(r => r.OrganisationalUnit)
                .WithMany(m => m.ManagedOrganisationalUnits)
                .HasForeignKey(f => f.OrganisationalUnitId);

            HasRequired(r => r.User)
                .WithMany(m => m.ManagedOrganisationalUnits)
                .HasForeignKey(f => f.UserId);

            ToTable("ManagedOrganisationalUnits", "usermgt");
        }
    }
}
