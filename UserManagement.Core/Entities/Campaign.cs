﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Entities
{
    public class Campaign : Base.BaseAuditableEntity<int>
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<OrganisationalUnit> OrganisationalUnits { get; set; }


        public Campaign()
        {
            OrganisationalUnits = new HashSet<OrganisationalUnit>();
        }
    }
}
