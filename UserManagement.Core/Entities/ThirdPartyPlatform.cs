﻿using System.Collections.Generic;

namespace UserManagement.Core.Entities
{
    public class ThirdPartyPlatform : Base.BaseAuditableEntity<int>
    {
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<UserThirdPartyLogin> UserThirdPartyLogins { get; set; }

        public ThirdPartyPlatform()
        {
            UserThirdPartyLogins = new HashSet<UserThirdPartyLogin>();
        }
    }
}
