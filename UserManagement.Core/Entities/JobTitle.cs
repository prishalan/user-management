﻿using System.Collections.Generic;

namespace UserManagement.Core.Entities
{
    public class JobTitle : Base.BaseAuditableEntity<int>
    {
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public JobTitle()
        {
            Users = new HashSet<User>();
        }
    }
}
