﻿using System.Collections.Generic;

namespace UserManagement.Core.Entities
{
    public class Company : Base.BaseAuditableEntity<int>
    {
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public Company()
        {
            Users = new HashSet<User>();
        }
    }
}
