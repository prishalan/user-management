﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Entities
{
    public class UserThirdPartyLogin : Base.BaseAuditableEntity<int>
    {
        public string Username { get; set; }
        public bool IsActive { get; set; }

        public int UserId { get; set; } // FK onto User
        public int PlatformId { get; set; } // FK onto ThirdPartyPlatform

        public virtual ThirdPartyPlatform Platform { get; set; }
        public virtual User User { get; set; }
    }
}
