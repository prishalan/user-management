﻿namespace UserManagement.Core.Entities
{
    public class ApiUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
    }
}
