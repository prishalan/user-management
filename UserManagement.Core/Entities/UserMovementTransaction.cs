﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Entities
{
    public class UserMovementTransaction : Base.BaseAuditableEntity<long>
    {
        public string Status { get; set; }
        public DateTime EffectiveDateStart { get; set; }
        public DateTime EffectiveDateEnd { get; set; }
        public string Note { get; set; }

        public int UserId { get; set; } // FK onto User (Employee)
        public int ManagerUserId { get; set; } // FK onto User (Manager)
        public int OrganisationalUnitId { get; set; } // FK onto OrganisationalUnit

        public virtual User ManagerUser { get; set; }
        public virtual OrganisationalUnit OrganisationalUnit { get; set; }
        public virtual User User { get; set; }
    }
}
