﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Entities
{
    public class ManagedOrganisationalUnit : Base.BaseAuditableEntity<int>
    {
        public DateTime EffectiveDate { get; set; }

        public int UserId { get; set; } // FK onto User
        public int OrganisationalUnitId { get; set; } // FK onto OrganisationalUnit

        public virtual OrganisationalUnit OrganisationalUnit { get; set; }
        public virtual User User { get; set; }
    }
}
