﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Entities
{
    public class OrganisationalUnit : Base.BaseAuditableEntity<int>
    {
        public string Title { get; set; }
        public string LdapPath { get; set; }
        public int? AssociatedCampaignId { get; set; }
        public bool IsActive { get; set; }

        public int? ParentId { get; set; } // FK onto self

        public virtual OrganisationalUnit Parent { get; set; }
        public virtual Campaign AssociatedCampaign { get; set; }

        public virtual ICollection<OrganisationalUnit> Children { get; set; }
        public virtual ICollection<ManagedOrganisationalUnit> ManagedOrganisationalUnits { get; set; }
        public virtual ICollection<UserMovementTransaction> UserMovementTransactions { get; set; }
        public virtual ICollection<User> Users { get; set; }

        public OrganisationalUnit()
        {
            Children                    = new HashSet<OrganisationalUnit>();
            ManagedOrganisationalUnits  = new HashSet<ManagedOrganisationalUnit>();
            UserMovementTransactions    = new HashSet<UserMovementTransaction>();
            Users                       = new HashSet<User>();
        }
    }
}
