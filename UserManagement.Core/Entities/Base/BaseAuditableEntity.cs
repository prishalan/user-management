﻿using System;

namespace UserManagement.Core.Entities.Base
{
    public abstract class BaseAuditableEntity<TType> : BaseEntity<TType> where TType : struct
    {
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
