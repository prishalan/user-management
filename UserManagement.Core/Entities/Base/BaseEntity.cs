﻿namespace UserManagement.Core.Entities.Base
{
    public abstract class BaseEntity<TType> where TType : struct
    {
        public virtual TType Id { get; set; }
    }
}
