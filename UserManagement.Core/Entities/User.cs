﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Entities
{
    public class User : Base.BaseAuditableEntity<int>
    {
        public User()
        {
            ManagedUsers                    = new HashSet<User>();
            ManagedOrganisationalUnits      = new HashSet<ManagedOrganisationalUnit>();
            UserManagerMovementTransactions = new HashSet<UserMovementTransaction>();
            UserMovementTransactions        = new HashSet<UserMovementTransaction>();
            UserThirdPartyLogins            = new HashSet<UserThirdPartyLogin>();
        }


        public string UserType { get; set; }
        public string EmployeeNumber { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string IdentityNumber { get; set; }
        public string PassportNumber { get; set; }
        public string Description { get; set; }
        public string ADCommonName { get; set; }
        public string ADSamAccountName { get; set; }
        public string ADDisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string TelephoneNumber { get; set; }
        public string IpPhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public DateTime? ManagerEffectiveDate { get; set; }
        public DateTime OrganisationalUnitEffectiveDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string TerminationReason { get; set; }
        public bool IsActive { get; set; }
        public int JobTitleId { get; set; } // FK onto JobTitle
        public int DepartmentId { get; set; } // FK onto Department
        public int CompanyId { get; set; } // FK onto Company
        public int? ManagerId { get; set; } // FK onto self
        public int OrganisationalUnitId { get; set; } //FK onto OrganisationalUnit


        public virtual JobTitle JobTitle { get; set; }
        public virtual Department Department { get; set; }
        public virtual Company Company { get; set; }
        public virtual User Manager { get; set; }
        public virtual OrganisationalUnit OrganisationalUnit { get; set; }


        public virtual ICollection<User> ManagedUsers { get; set; }
        public virtual ICollection<UserThirdPartyLogin> UserThirdPartyLogins { get; set; }
        public virtual ICollection<ManagedOrganisationalUnit> ManagedOrganisationalUnits { get; set; }
        public virtual ICollection<UserMovementTransaction> UserMovementTransactions { get; set; }
        public virtual ICollection<UserMovementTransaction> UserManagerMovementTransactions { get; set; }
        
    }
}
