﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Repository;

namespace UserManagement.Core.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        IApiUserRepository ApiUsers { get; }
        ICompanyRepository Companies { get; }
        IDepartmentRepository Departments { get; }
        IJobTitleRepository JobTitles { get; }
        IManagedOrganisationalUnitRepository ManagedOrganisationalUnits { get; }
        IOrganisationalUnitRepository OrganisationalUnits { get; }
        IThirdPartyPlatformRepository ThirdPartyPlatforms { get; }
        IUserRepository Users { get; }
        IUserMovementTransactionRepository UserMovementTransactions { get; }
        IUserThirdPartyLoginRepository UserThirdPartyLogins { get; }

        int Complete();
    }
}
