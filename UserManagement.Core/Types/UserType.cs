﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Types
{
    public static class UserType
    {
        public static string AGENT      = "AGENT";
        public static string BACKOFFICE = "BACKOFFICE";
    }
}
