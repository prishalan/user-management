﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Types
{
    public static class TransactionStatus
    {
        public const string NEW         = "NEW";
        public const string PENDING     = "PENDING";
        public const string SUCCEEDED   = "SUCCEEDED";
        public const string FAILED      = "FAILED";
        public const string CANCELLED   = "CANCELLED";
        public const string PAUSED      = "PAUSED";
    }
}
