﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Models
{
    public class GenericResponseModel<T>
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string MessageExtra { get; set; }
        public T Data { get; set; }
    }
}
