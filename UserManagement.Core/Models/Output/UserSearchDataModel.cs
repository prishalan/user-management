﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Models.Output
{
    public class UserSearchDataModel
    {
        public int UserId { get; set; }
        public string Fullname { get; set; }
        public string EmailAddress { get; set; }
    }
}
