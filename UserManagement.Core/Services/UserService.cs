﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Models;
using UserManagement.Core.Persistence;

namespace UserManagement.Core.Services
{
    public class UserService
    {
        private readonly IUnitOfWorkFactory _uowFactory;

        public UserService(IUnitOfWorkFactory uowFactory)
        {
            _uowFactory = uowFactory;
        }

        public GenericResponseModel<List<Models.Output.UserSearchDataModel>> GetActiveJuniorManagers(string jobTitle)
        {
            using (var uow = _uowFactory.Create())
            {
                var juniorManagers = uow.Users.GetAllUsersWithJobTitles()
                    .OrderBy(o => o.Firstname) 
                    .Where(x => x.JobTitle.Title == jobTitle && x.IsActive == true);

                return new GenericResponseModel<List<Models.Output.UserSearchDataModel>>()
                {
                    Success = true,
                    Message = "",
                    Data = juniorManagers.Select(s => new Models.Output.UserSearchDataModel()
                    {
                        UserId = s.Id,
                        Fullname = string.Format("{0} {1}", s.Firstname, s.Lastname),
                        EmailAddress = s.EmailAddress
                    }).ToList()
                };
            }
        }


        public GenericResponseModel<string> GetManagerCampaigns(int userId)
        {
            //var 

            return null;
        }
    }
}
