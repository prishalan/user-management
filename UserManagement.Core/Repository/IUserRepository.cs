﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Core.Entities;

namespace UserManagement.Core.Repository
{
    public interface IUserRepository : Base.IGenericRepository<Entities.User, int>
    {
        IEnumerable<User> GetAllUsersWithJobTitles();
    }
}
