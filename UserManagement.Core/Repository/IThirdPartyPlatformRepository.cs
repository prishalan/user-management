﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Core.Repository
{
    public interface IThirdPartyPlatformRepository : Base.IGenericRepository<Entities.ThirdPartyPlatform, int>
    {
    }
}
